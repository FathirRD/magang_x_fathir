def check(a, b):
    for n in list(range(a, b+1)):
        if n % 2 == 0:
            print(f'Angka {n} adalah genap')
        else:
            print(f'Angka {n} adalah ganjil')


if __name__ == "__main__":
    a = 0
    b = 0
    try:
        a = int(input("Masukan nilai A: "))
        b = int(input("Masukan nilai B: "))
    except:
        print("Nilai yang diinputkan bukan angka")
        exit()
    check(a, b)

