def check(str):
    vocal   ="aiueo"
    counter = {}
    str     = str.lower()
    # string = "".join(set(str))

    for s in vocal:
        count       = str.count(s)
        counter[s]  = count
    
    vowel = [k for k, v in counter.items() if v > 0]
    print(f'{len(vowel)} yaitu {" dan ".join(vowel)}')

if __name__ == '__main__':
    str = input('Masukan kata: ')
    check(str)