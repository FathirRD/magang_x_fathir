# Using buildin python eval()
def calculate(input):
    print(eval(input))

# From scratch
def kalkulator(input):
    val = input.split()
    if len(val) < 2 or len(val) > 3: 
        print('Not supported operation')
        exit()

    if val[1]   == '+':
        print(int(val[0]) + int(val[2]))
    
    elif val[1] == '-':
        print(int(val[0]) - int(val[2]))
    
    elif val[1] == '*' or val[1] == 'x' or val[1] == 'X':
        print(int(val[0]) * int(val[2]))

    elif val[1] == '/' or val[1] == ':':
        if val[2] == '0':
            print('Tidak bisa dilakukan')
            return
        print(int(val[0]) / int(val[2]))

    else:
        print('Not supported operation')
        return

if __name__ == '__main__':
    inp = input("Masukan nilai yang akan dihitung (contoh: 1 - 2): ")
    kalkulator(inp)