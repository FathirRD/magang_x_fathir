from django.contrib.auth    import get_user_model
from django.test            import TestCase

class UsersModelTestCase(TestCase):

    def test_create_user(self):
        User    = get_user_model
        user    = User.objects.create(email='test@user.com', password='asdf')
        # profil  = Profile.objects.get(user=user)

        """Model should create needed data"""
        self.assertEqual(user.email, 'test@user.com')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

        """Model should create profile data for related user when new user created"""
        # self.assertEqual(profil.user, user)

        """Model should return error when input data is invalid"""
        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(TypeError):
            User.objects.create_user(email="")
        with self.assertRaises(TypeError):
            User.objects.create_user(email="aaa", password="asdf")
    
    def test_create_superuser(self):
        User        = get_user_model()
        admin_user  = User.objects.create_superuser(email='test@admin.com', password="asdf76")
        # proifle     = Profile.objects.get(user=admin_user)

        """Model should create needed data"""
        self.assertEqual(admin_user.email, 'test@admin.com')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)

        with self.assertRaises(ValueError):
            User.objects.create_superuser(
                email='test@admin.com',
                password='asdf76',
                is_superuser=False
            )
        