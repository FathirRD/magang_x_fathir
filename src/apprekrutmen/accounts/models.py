from django.db                  import models
from django.contrib.auth.models import User


# Resume upload file
def resume_file(instance, filename):
    return f'{instance.user}/resume/{filename}'


class Profile(models.Model):
    user                = models.OneToOneField(User, on_delete=models.CASCADE)
    fullname            = models.CharField(max_length=255)
    email               = models.EmailField()
    current_company     = models.CharField(max_length=255)
    birthday            = models.DateField()
    birth_place         = models.CharField(max_lenght=255)
    gender              = models.CharField(
        max_length=5, 
        choices=(('M', 'Male'), ('F', 'Female'))
    )
    latest_education    = models.CharField(
        max_length=50,
        choices=(
            ('SLTA/SMA', 'SMA Sederajat'),
            ('Diploma', 'Diploma'),
            ('S1', 'Strata 1'),
            ('S2', 'Strata 2'),
            ('S3', 'Strata 3'),
        )    
    )
    join_date           = models.CharField(
        max_length=50,
        choices=(
            ('Immediatly', 'Immediatly'),
            ('1 Month Notice', '1 Month Notice'),
        )
    )
    job_prefer          = models.CharField(
        _("Which one you prefer?"), 
        max_length=50,
        choices=(
            ('WFH', 'Remote'),
            ('WFO', 'Onsite'),
            ('Both', 'Both')
        )
    )
    about               = models.TextField()
    portfolio           = models.TextField()
    linkedln            = models.TextField()
    how_know            = models.CharField(
        _("How do you know this oppurtinity?"), 
        max_length=50,
        choices=(
            ('Web', 'Web'),
            ('Group Chat', 'Group Chat'),
            ('Teman/Keluarga/Kenalan', 'Teman/Keluarga/Kenalan'),
            ('Search Engine', 'Search Engine'),
        )
    )
    smoker              = models.CharField(
        _("Are you smoker?"), 
        max_length=50,
        choices=(
            ('Y', 'Yes'),
            ('N', 'No'),
            ('YN', 'Yes but now No'),
        )
    )
    resume