from rest_framework import pagination

class RESTAPIPagination(pagination.LimitOffsetPagination):
    
    # page_size                   = 5
    default_limit               = 5
    max_limit                   = 50