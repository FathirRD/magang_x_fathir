from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .forms         import *
from .models        import *

# Register your models here.
class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form        = AdminChangeUserForm
    add_form    = AdminCreateUserForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display    = ['email', 'admin']
    list_filter     = ['admin']
    fieldsets = (
        (None, {'fields': ('email', 'password',)}),
        ('Personal info', {'fields': ('alamat', 'no_telp')}),
        ('Permissions', {'fields': ('admin', 'roles_id',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('nama', 'email', 'alamat', 'no_telp', 'password', 'password2', 'roles_id')}
        ),
    )
    search_fields       = ['email']
    ordering            = ['email']
    filter_horizontal   = ()
# class UserAdmin(admin.ModelAdmin):
#     list_display    = ('ID', 'nama', 'roles_id')
#     form            = AdminChangeUserForm
#     add_form        = AdminCreateUserForm

class RolesAdmin(admin.ModelAdmin):
    list_display    = ('ID', 'role')
    form            = UserRolesForm

class GenresAdmin(admin.ModelAdmin):
    list_display    = ('ID', 'genre')
    form            = BookGenreForm

class BooksAdmin(admin.ModelAdmin):
    list_display    = ('ID', 'judul_buku', 'genre_id', 'pemilik_id', 'peminjam_id')
    form            = AddBookForm

    def get_form(self, request, *args, **kwargs):
        form = super(BooksAdmin, self).get_form(request, *args, **kwargs)
        return form

admin.site.register(User,   UserAdmin)
admin.site.register(Roles,  RolesAdmin)
admin.site.register(Books,  BooksAdmin)