# Generated by Django 3.2.10 on 2021-12-13 06:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rental', '0003_auto_20211213_1307'),
    ]

    operations = [
        migrations.AlterField(
            model_name='books',
            name='deskripsi',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='books',
            name='gambar',
            field=models.FileField(blank=True, null=True, upload_to='gambar'),
        ),
    ]
