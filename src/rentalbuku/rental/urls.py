from django.urls import path
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import DeleteView

from .views import *

app_name = 'rental-buku'


urlpatterns = [
    path('', Index.as_view(), name='index'),
    path('register', UserRegisterView.as_view(), name='register'),
    path('login', UserLoginView.as_view(), name='login'),
    path('logout', UserLogoutView.as_view(), name='logout'),

    path(
        'list', 
        login_required(ListBookView.as_view(), login_url='rental-buku:login'), 
        name='list'
    ),
    path(
        'add', 
        login_required(CreateBookView.as_view(), login_url='rental-buku:login'), 
        name='create'
    ),
    path(
        '<int:ID>', 
        login_required(DetailBookView.as_view(), login_url='rental-buku:login'), 
        name='detail'
    ),
    path(
        'update/<int:ID>', 
        login_required(UpdateBookView.as_view(), login_url='rental-buku:login'), 
        name='update'
    ),
    path(
        'delete/<int:ID>', 
        login_required(DeleteBookView.as_view(), login_url='rental-buku:login'), 
        name='delete'
    ),
]