from django.db import models
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser


class Roles(models.Model):
    ID      = models.BigAutoField(primary_key=True)
    role    = models.CharField(max_length=20)

    def __str__(self) -> str:
        return self.role


class UserManager(BaseUserManager):
    def create_user(self, email, nama, no_telp, alamat, password=None):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            nama=nama, 
            no_telp=no_telp, 
            alamat=alamat, 
            password=password,
            roles_id=Roles.objects.get(ID=2),
            email=self.normalize_email(email)
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_staffuser(self, nama, no_telp, alamat, email, password):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            nama, no_telp, alamat, email, password
        )
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, nama, no_telp, alamat, email, password):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email, nama, no_telp, alamat, password,
        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser):
    ID          = models.BigAutoField(primary_key=True)
    nama        = models.CharField(max_length=100)
    email       = models.EmailField(unique=True)
    no_telp     = models.CharField(max_length=20)
    alamat      = models.TextField()
    password    = models.CharField(max_length=100)
    roles_id    = models.ForeignKey(Roles, on_delete=models.deletion.CASCADE)

    # BASE User Models
    is_active   = models.BooleanField(default=True)
    staff       = models.BooleanField(default=False)
    admin       = models.BooleanField(default=False)

    objects     = UserManager()

    USERNAME_FIELD  = 'email'
    REQUIRED_FIELDS = ['nama', 'no_telp', 'alamat', 'password'] 

    def get_full_name(self):
        return self.nama

    def get_short_name(self):
        return self.nama

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.staff

    @property
    def is_admin(self):
        "Is the user a admin member?"
        return self.admin


class Genre(models.Model):
    ID          = models.BigAutoField(primary_key=True)
    genre       = models.CharField(max_length=20)

    def __str__(self) -> str:
        return self.genre

class Books(models.Model):
    ID          = models.BigAutoField(primary_key=True)
    genre_id    = models.ForeignKey(Genre, on_delete=models.deletion.CASCADE)
    judul_buku  = models.CharField(max_length=100)
    deskripsi   = models.TextField(blank=True, null=True)
    gambar      = models.FileField(upload_to='gambar', blank=True, null=True)
    harga       = models.CharField(max_length=100)
    pemilik_id  = models.ForeignKey(User, related_name="pemilik", on_delete=models.deletion.CASCADE)
    peminjam_id = models.ForeignKey(User, related_name="peminjam",on_delete=models.deletion.CASCADE)
    status      = models.BooleanField(default=False)
    publish     = models.DateField()

    def __str__(self) -> str:
        return self.judul_buku
