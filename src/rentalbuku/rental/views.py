from django.shortcuts   import redirect, render
from django.views       import generic
from django.urls        import reverse_lazy
from django.contrib     import messages, auth

from .models            import *
from .forms             import *

'''
Users
'''
class UserRegisterView(generic.CreateView):
    model           = User
    form_class      = UserRegisterForm
    success_url     = reverse_lazy('rental-buku:index')
    template_name   = 'users/register.html'
    # success_message = "Registrasi berhasil"

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
    
    def form_invalid(self, form):
        return super(UserRegisterView, self).form_invalid(form)

class UserLoginView(generic.edit.FormView):
    form_class      = UserLoginForm
    template_name   = 'users/login.html'
    success_url     = reverse_lazy('rental-buku:index')

    def form_valid(self, form):
        email       = self.request.POST['email']
        password    = self.request.POST['password']
        user        = auth.authenticate(email=email, password=password)
        if user is not None:
            auth.login(self.request, user)
            return super().form_valid(form)
        else:
            return redirect('rental-buku:login')

    def form_invalid(self, form):
        return super(UserLoginView, self).form_invalid(form)

class UserLogoutView(generic.RedirectView):
    url = '/rental/login'

    def get(self, request, *args, **kwargs):
        auth.logout(request)
        return super(UserLogoutView, self).get(request, *args, **kwargs)

'''
Books
'''
class ListBookView(generic.ListView):

    model           = Books
    paginated_by    = 5
    template_name   = 'books/list.html'

class CreateBookView(generic.CreateView):
    
    form_class      = AddBookForm
    template_name   = 'books/create.html'
    success_url     = reverse_lazy('rental-buku:index')

    def get_form_kwargs(self):
        kwargs = super(CreateBookView, self).get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        print('nah')
        messages.error(self.request, 'nah')
        return super(CreateBookView, self).form_invalid(form)

class DetailBookView(generic.DetailView):
    
    slug_url_kwarg  = "ID"
    slug_field      = "ID"
    model           = Books
    template_name   = 'books/detail.html'

class UpdateBookView(generic.UpdateView):
    
    slug_url_kwarg  = "ID"
    slug_field      = "ID"
    model           = Books
    template_name   = 'books/update.html'
    success_url     = reverse_lazy('rental-buku:list')

    fields = [
        'judul_buku',
        'harga',
        'deskripsi',
        'status',
        'publish',
    ]

class DeleteBookView(generic.DeleteView):
    
    slug_url_kwarg  = "ID"
    slug_field      = "ID"
    model           = Books
    template_name   = 'books/delete.html'
    success_url     = reverse_lazy('rental-buku:list')

class Index(generic.View):
    
    def get(self, request):
        return render(request, 'index.html')

