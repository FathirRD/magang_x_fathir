import datetime
from django                     import forms
from django.core.exceptions     import ValidationError
from django.contrib.auth        import authenticate, login
from django.contrib.auth.forms  import ReadOnlyPasswordHashField
from django.forms.models        import ModelChoiceField
from django.forms.widgets       import CheckboxInput, ChoiceWidget, DateInput, DateTimeInput, FileInput, HiddenInput, NumberInput, PasswordInput, RadioSelect, Select, SelectMultiple, TextInput, Textarea

from .models import *

# Form yang diperlukan adalah nama ,email, nomor telepon, alamat, dan password 
class UserRegisterForm(forms.ModelForm):
    
    # Edit fields in constructor
    # def __init__(self, *args, **kwargs):
    #     super(UserRegisterForm, self).__init__(*args, **kwargs)

    nama        = forms.CharField(
        max_length=100,
        widget=TextInput({'class':'forms-control'}), 
        required=True
    )
    email       = forms.EmailField(
        required=True,
        widget=TextInput({'class':'forms-control'}), 
    )
    no_telp     = forms.CharField(
        required=True,
        widget=NumberInput({'class':'forms-control'}),  
        label="Nomor Telepon"
    )
    alamat      = forms.CharField(
        widget=Textarea({'class': 'forms-control'}), 
        required=True
    )
    password    = forms.CharField(
        min_length=8,
        widget=PasswordInput({'class': 'forms-control'}),  
        required=True
    )
    password2   = forms.CharField(
        widget=PasswordInput({'class': 'forms-control'}),
        required=True,
        label="Konfirmasi password"
    )
    roles_id    = forms.CharField(
        required=False, 
        widget=HiddenInput()
    )

    class Meta:
        model   = User
        fields  = '__all__'

    def clean(self, *args, **kwargs):
        cleaned_data    = super().clean(*args, **kwargs)
        cleaned_data['roles_id'] = Roles.objects.get(ID=2)
        email           = cleaned_data.get('email')
        password        = cleaned_data.get('password')
        password2       = cleaned_data.get('password2')
        
        if User.objects.filter(email__iexact=email).exists():
            raise ValidationError("Email telah terdaftar")

        if password2 != password:
            raise ValidationError("Password tidak sama")

    def save(self, commit=True):
        user = super(UserRegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

class UserLoginForm(forms.Form):

    email       = forms.EmailField(
            required=True,
            widget=TextInput({'class':'forms-control'}), 
        )

    password    = forms.CharField(
            widget=PasswordInput({'class': 'forms-control'}),  
            required=True
        )

    def clean(self, *args, **kwargs):
        email           = self.cleaned_data.get('email')
        password        = self.cleaned_data.get('password')

        user            = authenticate(email=email, password=password)

        if not user:
            raise ValidationError('email atau password salah')
        

class AddBookForm(forms.ModelForm):

    genre_id    = forms.ModelChoiceField(
        queryset=Genre.objects.all(),
        initial=Genre.objects.get(ID=999),
        widget=Select({'class':'form-control'}),
        label="Genre buku"
    )
    judul_buku  = forms.CharField(
        max_length=100,
        widget=TextInput({'class':'form-control'}),
        required=True,
        label="Judul buku"
    )
    deskripsi   = forms.CharField(
        widget=Textarea({'class':'form-control'}),
        required=False
    )
    gambar      = forms.FileField(
        widget=FileInput({'class':'form-control-file'}),
        required=False
    )
    harga       = forms.CharField(
        widget=NumberInput({'class':'form-control'})
    )
    peminjam_id = forms.ModelChoiceField(
        queryset=User.objects.all(),
        widget=Select({'class':'form-control'}),
        label="Peminjam"
    )
    status      = forms.BooleanField(
        label='Sedang dipinjam',
        required=False
    )
    publish     = forms.DateField(
        initial=datetime.datetime.now().strftime("%Y-%m-%d"), 
        widget=DateInput(attrs={'type': 'date'})
    )
    pemilik_id    = forms.CharField(
        required=False, 
        widget=HiddenInput()
    )
    

    class Meta:
        model   = Books
        fields  = '__all__'

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(AddBookForm, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        cleaned_data    = super().clean(*args, **kwargs)
        if not self.user:
            raise ValidationError('Hmmmm')
        if not cleaned_data['peminjam_id']:
            cleaned_data['peminjam_id'] = User.objects.get(ID=self.user.ID)
        cleaned_data['pemilik_id'] = User.objects.get(ID=self.user.ID)

        print(cleaned_data)

'''
Admin Forms
'''
class UserRolesForm(forms.ModelForm):

    role        = forms.CharField()

    class Meta:
        model   = User
        fields  = [
            'role'
        ]

class BookGenreForm(forms.ModelForm):

    genre        = forms.CharField()

    class Meta:
        model   = Genre
        fields  = [
            'genre'
        ]

class AdminCreateUserForm(forms.ModelForm):
    password   = forms.CharField(
            widget=PasswordInput({'class': 'forms-control'}),
            required=True,
            label="Konfirmasi password"
        )
    password2   = forms.CharField(
            widget=PasswordInput({'class': 'forms-control'}),
            required=True,
            label="Konfirmasi password"
        )
    class Meta:
        model   = User
        fields  = '__all__'

    def clean(self):
        cleaned_data    = super().clean()
        password        = cleaned_data.get("password")
        password2       = cleaned_data.get("password2")
        if password is not None and password != password2:
            raise ValidationError("Password tidak sama")
        return cleaned_data

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

class AdminChangeUserForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField(
            help_text='<a href="../password/">Change</a>'
        )
    
    class Meta:
        model = User
        fields = '__all__'

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]